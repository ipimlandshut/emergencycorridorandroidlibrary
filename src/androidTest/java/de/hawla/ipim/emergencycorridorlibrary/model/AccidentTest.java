package de.hawla.ipim.emergencycorridorlibrary.model;

import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import de.hawla.ipim.emergencycorridorlibrary.model.Accident;
import de.hawla.ipim.emergencycorridorlibrary.model.GPSLocation;

/**
 * Created by Julian on 30.06.2017.
 */
@RunWith(AndroidJUnit4.class)
@SmallTest
public class AccidentTest {

    private static final String TAG = "AccidentTest";


    @Test
    public void getDistanceBetweenAccidentAndEndOfTrafficJam() throws Exception {
        GPSLocation accidentLocation = new GPSLocation(47.906903, 11.395785);
        GPSLocation jamEndLocation = new GPSLocation(47.902501, 11.397158);

        Accident testAccident = new Accident(accidentLocation, jamEndLocation);
        float distanceInMeter = testAccident.getDistanceBetweenAccidentAndEndOfTrafficJam();
        Log.i("Distance is: " + distanceInMeter + "m", TAG);
        Assert.assertEquals(500, distanceInMeter, 0.1);
    }

}