package de.hawla.ipim.emergencycorridorlibrary.service;

import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import de.hawla.ipim.emergencycorridorlibrary.model.Accident;
import de.hawla.ipim.emergencycorridorlibrary.model.GPSLocation;
import de.hawla.ipim.emergencycorridorlibrary.model.UserData;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Julian Dörndorfer on 30.06.2017.
 *
 */
@RunWith(AndroidJUnit4.class)
@MediumTest
public class AlertServiceTest {

    private static final String TAG = "AlertServiceTest";

    private Accident testAccident;
    private AlertService alertService;
    private UserData userData;

    @Before
    public void setUp() throws Exception {
        GPSLocation accidentLocation = new GPSLocation(47.906903, 11.395785);
        GPSLocation jamEndLocation = new GPSLocation(47.90575, 11.39623);

        testAccident = new Accident(accidentLocation,jamEndLocation);
        alertService = new AlertService();
        userData = new UserData();

    }

    /**
     * This method tests the method isUserRelevantForAccident with a GPS position before the traffic jam, within the concerning radius and the right side
     * @throws Exception
     */
    @Test
    public void isUserCloseToAccidentAreaInRange() throws Exception {
        GPSLocation userLocation = new GPSLocation(47.897291, 11.397021);
        boolean isRelevant = alertService.isUserCloseToAccidentArea(testAccident, userLocation);
        Log.d("isRelevant? " + isRelevant, TAG);
        assertTrue(isRelevant);

    }

    /**
     * This method tests the method isUserRelevantForAccident with a GPS position before the traffic jam, outside the concerning radius and the right side
     * @throws Exception
     */
    @Test
    public void isUserCloseToAccidentAreaOutOfRange() throws Exception {
        GPSLocation userLocation = new GPSLocation(47.803086, 11.361369);
        boolean isRelevant = alertService.isUserCloseToAccidentArea(testAccident, userLocation);
        Log.d("isRelevant? " + isRelevant, TAG);
        assertFalse(isRelevant);
    }

    /**
     * This method tests the method isUserRelevantForAccident GPS position is:
     *  - correct side
     *  - correct road
     *  - before traffic jam
     *
     *  Expected value = true
     * @throws Exception
     */
    @Test
    public void isUserRelevantForAccidentBeforeJamCorrectSide() throws Exception {
        GPSLocation userLocation1 = new GPSLocation(47.90037, 11.39715);
        GPSLocation userLocation2 = new GPSLocation(47.90040, 11.39716);
        GPSLocation userLocation3 = new GPSLocation(47.90043, 11.39715);
        List<GPSLocation> userLocations = new ArrayList<>();
        userLocations.add(userLocation1);
        userLocations.add(userLocation2);
        userLocations.add(userLocation3);
        userData.setUserLocations(userLocations);

        boolean isRelevant = alertService.isUserRelevantForAccident(testAccident, userData);
        Log.d("isRelevant? " + isRelevant, TAG);
        assertTrue(isRelevant);
    }

    /**
     * This method tests the method isUserRelevantForAccident GPS position is:
     *  - correct side
     *  - correct road
     *  - in traffic jam but closer to the end of the jam than to the accident point
     *
     *  Expected value = true
     * @throws Exception
     */
    @Test
    public void isUserRelevantForAccidentInJamCloserToEndOfJamCorrectSide() throws Exception {
        GPSLocation userLocation1 = new GPSLocation(47.90296, 11.39704);
        GPSLocation userLocation2 = new GPSLocation(47.90345, 11.39695);
        GPSLocation userLocation3 = new GPSLocation(47.90361, 11.39694);
        List<GPSLocation> userLocations = new ArrayList<>();
        userLocations.add(userLocation1);
        userLocations.add(userLocation2);
        userLocations.add(userLocation3);
        userData.setUserLocations(userLocations);

        boolean isRelevant = alertService.isUserRelevantForAccident(testAccident, userData);
        Log.d("isRelevant? " + isRelevant, TAG);
        assertTrue(isRelevant);
    }

    /**
     * This method tests the method isUserRelevantForAccident GPS position is:
     *  - correct side
     *  - correct road
     *  - in traffic jam but closer to the accident than to the accident point
     *
     *  Expected value = true
     * @throws Exception
     */
    @Test
    public void isUserRelevantForAccidentInJamCloserToAccidentCorrectSide() throws Exception {
        GPSLocation userLocation1 = new GPSLocation(47.90536, 11.39638);
        GPSLocation userLocation2 = new GPSLocation(47.90549, 11.39634);
        GPSLocation userLocation3 = new GPSLocation(47.90563, 11.39628);
        List<GPSLocation> userLocations = new ArrayList<>();
        userLocations.add(userLocation1);
        userLocations.add(userLocation2);
        userLocations.add(userLocation3);
        userData.setUserLocations(userLocations);
        userData.setSpeed(5f);

        boolean isRelevant = alertService.isUserRelevantForAccident(testAccident, userData);
        Log.d("isRelevant? " + isRelevant, TAG);
        assertTrue(isRelevant);
    }

    /**
     * This method tests the method isUserRelevantForAccident GPS position is:
     *  - correct side
     *  - correct road
     *  - after accident point
     *
     *  Expected value = false
     * @throws Exception
     */
    @Test
    public void isUserRelevantAfterAccidentCorrectSide() throws Exception {
        GPSLocation userLocation1 = new GPSLocation(47.90732, 11.39557);
        GPSLocation userLocation2 = new GPSLocation(47.90770, 11.39545);
        GPSLocation userLocation3 = new GPSLocation(47.90813, 11.39528);
        List<GPSLocation> userLocations = new ArrayList<>();
        userLocations.add(userLocation1);
        userLocations.add(userLocation2);
        userLocations.add(userLocation3);
        userData.setUserLocations(userLocations);
        userData.setSpeed(50f);

        boolean isRelevant = alertService.isUserRelevantForAccident(testAccident, userData);
        Log.d("isRelevant? " + isRelevant, TAG);
        assertFalse(isRelevant);
    }


    /**
     * This method tests the method isUserRelevantForAccident GPS position is:
     *  - wrong side
     *  - correct road
     *  - before accident point
     *
     *  Expected value = false
     * @throws Exception
     */
    @Test
    public void isUserRelevantBeforeAccidentWrongSide() throws Exception {
        GPSLocation userLocation1 = new GPSLocation(47.90801, 11.39512);
        GPSLocation userLocation2 = new GPSLocation(47.90771, 11.39526);
        GPSLocation userLocation3 = new GPSLocation(47.90736, 11.39539);
        List<GPSLocation> userLocations = new ArrayList<>();
        userLocations.add(userLocation1);
        userLocations.add(userLocation2);
        userLocations.add(userLocation3);
        userData.setUserLocations(userLocations);
        userData.setSpeed(50f);

        boolean isRelevant = alertService.isUserRelevantForAccident(testAccident, userData);
        Log.d("isRelevant? " + isRelevant, TAG);
        assertFalse(isRelevant);
    }


    /**
     * This method tests the method isUserRelevantForAccident GPS position is:
     *  - correct wrong side
     *  - correct road
     *  - closer to accident point than to the end of the traffic jam
     *
     *  Expected value = false
     * @throws Exception
     */
    @Test
    public void isUserRelevantForAccidentAfterAccidentWrongSideCloserToAccidentPoint() throws Exception {
        GPSLocation userLocation1 = new GPSLocation(47.90708, 11.39551);
        GPSLocation userLocation2 = new GPSLocation(47.90685, 11.39560);
        GPSLocation userLocation3 = new GPSLocation(47.90662, 11.39569);
        List<GPSLocation> userLocations = new ArrayList<>();
        userLocations.add(userLocation1);
        userLocations.add(userLocation2);
        userLocations.add(userLocation3);
        userData.setUserLocations(userLocations);

        boolean isRelevant = alertService.isUserRelevantForAccident(testAccident, userData);
        Log.d("isRelevant? " + isRelevant, TAG);
        assertFalse(isRelevant);
    }

    /**
     * This method tests the method isUserRelevantForAccident GPS position is:
     *  - correct wrong side
     *  - correct road
     *  - closer to the end of the traffic jam than to accident point
     *
     *  Expected value = false
     * @throws Exception
     */
    @Test
    public void isUserRelevantForAccidentAfterAccidentWrongSideCloserToEndOfJam() throws Exception {
        GPSLocation userLocation1 = new GPSLocation(47.90470, 11.39642);
        GPSLocation userLocation2 = new GPSLocation(47.90444, 11.39653);
        GPSLocation userLocation3 = new GPSLocation(47.90399, 11.39657);
        List<GPSLocation> userLocations = new ArrayList<>();
        userLocations.add(userLocation1);
        userLocations.add(userLocation2);
        userLocations.add(userLocation3);
        userData.setUserLocations(userLocations);

        boolean isRelevant = alertService.isUserRelevantForAccident(testAccident, userData);
        Log.d("isRelevant? " + isRelevant, TAG);
        assertFalse(isRelevant);
    }

    /**
     * This method tests the method isUserRelevantForAccident GPS position is:
     *  - correct wrong side
     *  - correct road
     *  - after jam
     *
     *  Expected value = false
     * @throws Exception
     */
    @Test
    public void isUserRelevantForAccidentAfterJam() throws Exception {
        GPSLocation userLocation1 = new GPSLocation(47.90206, 11.39695);
        GPSLocation userLocation2 = new GPSLocation(47.90176, 11.39696);
        GPSLocation userLocation3 = new GPSLocation(47.90113, 11.39698);
        List<GPSLocation> userLocations = new ArrayList<>();
        userLocations.add(userLocation1);
        userLocations.add(userLocation2);
        userLocations.add(userLocation3);
        userData.setUserLocations(userLocations);

        boolean isRelevant = alertService.isUserRelevantForAccident(testAccident, userData);
        Log.d("isRelevant? " + isRelevant, TAG);
        assertFalse(isRelevant);
    }

}