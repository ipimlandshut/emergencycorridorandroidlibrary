package de.hawla.ipim.emergencycorridorlibrary.service;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import de.hawla.ipim.emergencycorridorlibrary.model.Accident;
import de.hawla.ipim.emergencycorridorlibrary.model.GPSLocation;

import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class MapMatchingServiceTest {


    @Test
    public void  getStreetFromGPSPositions(){
        List<GPSLocation> gpsLocations = new ArrayList<>();
        gpsLocations.add(new GPSLocation(47.897238, 11.397070));
        gpsLocations.add(new GPSLocation(47.898806, 11.397070));
        gpsLocations.add(new GPSLocation(47.900834, 11.397199));
        gpsLocations.add(new GPSLocation(47.902481, 11.397081));
//        gpsLocations.add(new GPSLocation(47.903688, 11.396847));
//        gpsLocations.add(new GPSLocation(47.902501, 11.397158));
//        gpsLocations.add(new GPSLocation(47.901070, 11.397147));
        String street = MapMatchingService.getStreetFromGPSPositions(gpsLocations);
        Assert.assertFalse(street.isEmpty());
        Assert.assertEquals("A 95", street);
    }
}
