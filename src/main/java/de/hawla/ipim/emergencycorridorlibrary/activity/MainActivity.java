package de.hawla.ipim.emergencycorridorlibrary.activity;

import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import haw.landshut.de.emergencycorridorlibrary.R;

public class MainActivity extends AppCompatActivity {

    TextToSpeech textToSpeech;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onCloseButtonClick(View view) {
        finish();
    }
}
