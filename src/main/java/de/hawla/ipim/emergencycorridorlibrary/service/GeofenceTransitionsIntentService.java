package de.hawla.ipim.emergencycorridorlibrary.service;

import android.Manifest;
import android.app.ActivityManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingEvent;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hawla.ipim.emergencycorridorlibrary.activity.MainActivity;
import de.hawla.ipim.emergencycorridorlibrary.model.Accident;
import de.hawla.ipim.emergencycorridorlibrary.model.Constants;
import de.hawla.ipim.emergencycorridorlibrary.model.GPSLocation;
import de.hawla.ipim.emergencycorridorlibrary.model.GeofenceErrorMessages;
import de.hawla.ipim.emergencycorridorlibrary.model.UserData;
import haw.landshut.de.emergencycorridorlibrary.R;

/**
 * Created by Julian on 02.08.2017.
 * This class handles the geofence transitions.
 */

public class GeofenceTransitionsIntentService extends IntentService {
    private static final String TAG = "GeofenceTransitionsIS";
    private LocationManager locationManager;
    private UserData userData;
    boolean invokeFirstTime;
    private AlertService alertService;
    private Accident accidentInCloseDistance;
    private TextToSpeech textToSpeech;
    private String fineLocationProvider;
    private List<Geofence> triggeringGeofences;
    private HashMap<String, Accident> accidentsMappedAsGeofences;

    /**
     * This constructor is required, and calls the super IntentService(String)
     * constructor with the name for a worker thread.
     */
    public GeofenceTransitionsIntentService() {
        // Use the TAG to name the worker thread.
        super(TAG);
    }

    /**
     * Handles incoming intents.
     * @param intent sent by Location Services. This Intent is provided to Location
     *               Services (inside a PendingIntent) when addGeofences() is called.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        Log.d(TAG, "Geofence event occurred with state: " + geofencingEvent.getGeofenceTransition());
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        if (geofencingEvent.hasError()) {
            String errorMessage = GeofenceErrorMessages.getErrorString(this,
                    geofencingEvent.getErrorCode());
            Log.e(TAG, errorMessage);
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {

            // Get the geofences that were triggered. A single event can trigger multiple geofences.
            triggeringGeofences = geofencingEvent.getTriggeringGeofences();
            for (Geofence currentGeofence : triggeringGeofences) {
                accidentsMappedAsGeofences = EmergencyCorridorMessageService.getAccidentsFromPrefs(getApplicationContext());
                accidentInCloseDistance = accidentsMappedAsGeofences.get(currentGeofence.getRequestId());
                if (accidentInCloseDistance != null) {
                    Log.d(TAG, "Matched accident: " + accidentInCloseDistance);
                    invokeFirstTime = true;
                    userData = new UserData();
                    Criteria criteria = new Criteria();
                    fineLocationProvider = locationManager.getBestProvider(criteria, true);
                    criteria.setAccuracy(Criteria.ACCURACY_FINE);
                    LocationListener locationListener = getLocationListener();
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    locationManager.requestLocationUpdates(fineLocationProvider, Constants.minTime, Constants.minDistance, locationListener, getMainLooper());
                }
            }
            // Get the transition details as a String.
            String geofenceTransitionDetails = getGeofenceTransitionDetails(geofenceTransition,
                    triggeringGeofences);
        }
    }

    /**
     * Gets transition details and returns them as a formatted string.
     *
     * @param geofenceTransition    The ID of the geofence transition.
     * @param triggeringGeofences   The geofence(s) triggered.
     * @return                      The transition details formatted as String.
     */
    private String getGeofenceTransitionDetails(
            int geofenceTransition,
            List<Geofence> triggeringGeofences) {

        String geofenceTransitionString = getTransitionString(geofenceTransition);

        // Get the Ids of each geofence that was triggered.
        ArrayList<String> triggeringGeofencesIdsList = new ArrayList<>();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesIdsList.add(geofence.getRequestId());
        }
        String triggeringGeofencesIdsString = TextUtils.join(", ",  triggeringGeofencesIdsList);

        return geofenceTransitionString + ": " + triggeringGeofencesIdsString;
    }

    @NonNull
    private LocationListener getLocationListener() {
        return new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d(TAG,"Provider enabled: " + provider);
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d(TAG, "Provider disabled: " + provider);
            }

            @Override
            public void onLocationChanged(Location location) {

                // Do work with new location. Implementation of this method will be covered later.
                if(userData.getUserLocations().size()<5){
                    GPSLocation currentLocation = new GPSLocation(location.getLatitude(), location.getLongitude());
                    userData.getUserLocations().add(currentLocation);
                }else if(userData.getUserLocations().size()==5 && invokeFirstTime){
                    invokeFirstTime = false;
                    locationManager.removeUpdates(this);
                    final Intent launchIntent;
                    if (isAppIsInBackground()) {
                        launchIntent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(getApplicationContext().getPackageName());
                        launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    } else {
                        launchIntent = new Intent(getApplicationContext(), MainActivity.class);
                    }
                    alertService = new AlertService();
                    //without a thread the application would throw a android.os.NetworkOnMainThreadException
                    Thread thread = checkForAccidentsOnUsersWay(launchIntent);
                    thread.start();
                }
            }
        };
    }

    @NonNull
    private Thread checkForAccidentsOnUsersWay(final Intent launchIntent) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
                try  {
                    if(alertService.isUserRelevantForAccident(accidentInCloseDistance, userData)) {
                        Log.d(TAG, "currentAccident relevant for user " + accidentInCloseDistance);
                        String notificationString = prepareEmergencyNotification(accidentInCloseDistance);
                        announceCorridorMessage(notificationString);
                        startActivity(launchIntent);
                        removeCurrentGeofence();
                        accidentsMappedAsGeofences.remove(getPackageName() + "." + accidentInCloseDistance.getId());
                        EmergencyCorridorMessageService.addGeofencesToMap(getApplicationContext(), accidentsMappedAsGeofences);
                    } else if(alertService.isUserCloseToAccidentArea(accidentInCloseDistance, userData.getUserLocations().get(userData.getUserLocations().size()-1)) &&
                            isAccidentStillRelevant(accidentInCloseDistance.getTimeInMilliSec())){
                        Log.d(TAG, "currentAccident " + accidentInCloseDistance.getId() + " not relevant for user. Draw closer circle around traffic jam end.");
                        removeCurrentGeofence();
                        addNarrowedGeofence();
                    }
                } catch (Exception e) {
                    Log.w(TAG, e.toString(), e);
                    Log.w(TAG, "Could not inform user to build a emergency corridor because of an exception.", e);
                }
            }
        });
    }

    private boolean isAccidentStillRelevant(long accidentTimeStampInMilliSec){
        long timeDifferenceInMilliSec = System.currentTimeMillis() - accidentTimeStampInMilliSec;
        return timeDifferenceInMilliSec < Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS;

    }

    private void removeCurrentGeofence() {
        GeofencingClient geofencingClient = LocationServices.getGeofencingClient(getApplicationContext());
        List<String> geofenceListToRemove = new ArrayList<>();
        geofenceListToRemove.add(accidentInCloseDistance.getId());
        geofencingClient.removeGeofences(geofenceListToRemove);
    }

    private void addNarrowedGeofence() {
        Geofence geofence = new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId(accidentInCloseDistance.getId())

                // Set the circular region of this geofence.
                .setCircularRegion(
                        accidentInCloseDistance.getTrafficJamEndLocation().getLatitude(),
                        accidentInCloseDistance.getTrafficJamEndLocation().getLongitude(),
                        Constants.WARNING_AREA_AFTER_TRAFFIC_JAM
                )

                // Set the expiration duration of the geofence. This geofence gets automatically
                // removed after this period of time.
                .setExpirationDuration(Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)

                // Set the transition types of interest. Alerts are only generated for these
                // transition. We track entry and exit transitions in this sample.
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                // Create the geofence.
                .build();
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofence(geofence);
    }

    private boolean isAppIsInBackground() {
        boolean isInBackground = true;
        Context context = getApplicationContext();
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    private String prepareEmergencyNotification(Accident currentAccident) {
        String notificationString = getResources().getString(R.string.emergency_corridor_text);
        notificationString = notificationString.replace("X", currentAccident.getExitNearbyAccident());
        notificationString = notificationString.replace("Y", currentAccident.getExitNearbyJamEnd());
        sendNotification(getResources().getString(R.string.geofence_notification_title), notificationString);
        return notificationString;
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     * @param title FCM title recived
     */
    private void sendNotification(String title, String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.emergency_corridor_icon_bw)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    /**
     * Maps geofence transition types to their human-readable equivalents.
     *
     * @param transitionType    A transition type constant defined in Geofence
     * @return                  A String indicating the type of transition
     */
    private String getTransitionString(int transitionType) {
        switch (transitionType) {
            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return getString(R.string.geofence_transition_entered);
            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofence_transition_exited);
            default:
                return getString(R.string.unknown_geofence_transition);
        }
    }
    public void announceCorridorMessage(final String speechString) {
        Log.d("InformationActivity", "application context\n" + getApplicationContext());
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    textToSpeech.speak(speechString, TextToSpeech.QUEUE_FLUSH, null);
                } else {
                    Log.e(TAG, "TextToSpeech not successful");
                }
            }
        });
    }
}
