/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.hawla.ipim.emergencycorridorlibrary.service;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hawla.ipim.emergencycorridorlibrary.activity.MainActivity;
import de.hawla.ipim.emergencycorridorlibrary.model.Accident;
import de.hawla.ipim.emergencycorridorlibrary.model.Constants;
import de.hawla.ipim.emergencycorridorlibrary.model.GPSLocation;
import de.hawla.ipim.emergencycorridorlibrary.model.UserData;
import haw.landshut.de.emergencycorridorlibrary.R;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private TextToSpeech textToSpeech;
    private AlertService alertService;
    private UserData userData;
    private LocationManager locationManager;
    private List<Accident> accidentList;
    private List<Geofence> geofenceAccidentList;
    private HashMap<String, Accident> accidentsMappedAsGeofences;
    private String titel;
    boolean invokeFirstTime;
    /**
     * Provides access to the Geofencing API.
     */
    private GeofencingClient geofencingClient;
    /**
     * Used when requesting to add or remove geofences.
     */
    private PendingIntent geofencePendingIntent;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> remoteMessageData = remoteMessage.getData();
        accidentList = getAccidents(remoteMessageData);
        if(accidentList != null && !accidentList.isEmpty()) {
            accidentsMappedAsGeofences = EmergencyCorridorMessageService.getAccidentsFromPrefs(getApplicationContext());
            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            userData = new UserData();
            invokeFirstTime = true;
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            geofenceAccidentList = new ArrayList<>();
            geofencingClient = LocationServices.getGeofencingClient(getApplicationContext());
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                Log.d(TAG, "no permission to get gps location");
                return;
            }
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                for (Accident currentAccident : accidentList) {
                    addCurrentAccidentToGeofenceList(currentAccident);
                    accidentsMappedAsGeofences.put(getPackageName() + "." + currentAccident.getId(), currentAccident);
                }
                geofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent());
                EmergencyCorridorMessageService.addGeofencesToMap(getApplicationContext(), accidentsMappedAsGeofences);
                Log.d(TAG, "gps is deactivated. Accidents will be added to geofences.");
                return;
            }

            titel = remoteMessageData.get(getResources().getString(R.string.message_key_title));
            // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
            Log.d(TAG, "Message from: " + remoteMessage.getFrom());

            LocationListener locationListener = getLocationListener();

            // Assign LocationListener to LocationManager in order to receive location updates with a delay of 1 sec.
            // This is necessary to get at least 5 gps points to match the user with a street
            // Additionally provided the MainLooper to avoid this exception "java.lang.RuntimeException: Can't create handler inside thread that has not called Looper.prepare()"
            String provider = locationManager.getBestProvider(criteria, false);
            locationManager.requestLocationUpdates(provider, Constants.minTime, Constants.minDistance, locationListener, getMainLooper());


        }
    }

    /**
     * The location listener
     * @return LocationListener
     */
    @NonNull
    private LocationListener getLocationListener() {
        return new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d(TAG, "Provider enabled: " + provider);
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d(TAG, "Provider disabled: " + provider);
            }

            /**
             * Will be called five times. At the fifth time the app will calculate whether or not the user position is relevant to an accident.
             *
             * @param location current location
             */
            @Override
            public void onLocationChanged(Location location) {

                if (userData.getUserLocations().size() < 5) {
                    GPSLocation currentLocation = new GPSLocation(location.getLatitude(), location.getLongitude());
                    userData.getUserLocations().add(currentLocation);
                } else if (userData.getUserLocations().size() == 5 && invokeFirstTime) {
                    locationManager.removeUpdates(this);
                    invokeFirstTime = false;
                    final Intent launchIntent;
                    if (isAppIsInBackground()) {
                        launchIntent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(getApplicationContext().getPackageName());
                        launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    } else {
                        launchIntent = new Intent(getApplicationContext(), MainActivity.class);
                    }
                    alertService = new AlertService();
                    //without a thread the application would throw a android.os.NetworkOnMainThreadException
                    Thread thread = checkForAccidentsOnUsersWay(launchIntent);
                    thread.start();
                }
            }
        };
    }

    /**
     *
     * This method checks whether a accident is relevant to the users position or not.
     * If so, a hint for the emergency corridor will be displayed and a message will be read.
     * If not, a geofence will be drawn to inform the user when he is near the jam.
     * @param launchIntent intent to fire
     * @return thread to execute
     */
    @NonNull
    private Thread checkForAccidentsOnUsersWay(final Intent launchIntent) {
        return new Thread(new Runnable() {
            @Override
            public void run() {
            try {
                for (Accident currentAccident : accidentList) {
                    Log.d(TAG, "User gps position: " + userData.getUserLocations());
                    if (alertService.isUserRelevantForAccident(currentAccident, userData)) {
                        Log.d(TAG, "currentAccident " + currentAccident.getId() +" relevant for user " + currentAccident);
                        String notificationString = prepareEmergencyNotification(currentAccident);
                        announceCorridorMessage(notificationString);
                        startActivity(launchIntent);
                        break;
                    } else {
                        Log.d(TAG, "Accident " + currentAccident.getId() + " is at the moment not relevant for user " + userData);
                        Log.d(TAG, "Add accident to geofence list");
                        addCurrentAccidentToGeofenceList(currentAccident);
                        accidentsMappedAsGeofences.put(getPackageName() + "." + currentAccident.getId(), currentAccident);
                    }
                }
                geofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent());
                EmergencyCorridorMessageService.addGeofencesToMap(getApplicationContext(), accidentsMappedAsGeofences);
            } catch (Exception e) {
                e.printStackTrace();
            }
            }
        });
    }

    /**
     * This method prepares the message to announce by replacing X and Y by real exit names.
     * @param currentAccident current accident
     * @return message to announce
     */
    private String prepareEmergencyNotification(Accident currentAccident) {
        String notificationString = getResources().getString(R.string.emergency_corridor_text);
        notificationString = notificationString.replace("X", currentAccident.getExitNearbyAccident());
        notificationString = notificationString.replace("Y", currentAccident.getExitNearbyJamEnd());
        sendNotification(titel, notificationString);
        return notificationString;
    }

    /**
     * Adds a accident to geofence list.
     * @param currentAccident the accident to add
     */
    private void addCurrentAccidentToGeofenceList(Accident currentAccident) {
        geofenceAccidentList.add(new Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId(getPackageName() + "." + currentAccident.getId())
                // Set the circular region of this geofence.
                .setCircularRegion(
                        currentAccident.getAccidentLocation().getLatitude(),
                        currentAccident.getAccidentLocation().getLongitude(),
                        currentAccident.getDistanceBetweenAccidentAndEndOfTrafficJam()+ Constants.WARNING_AREA_AFTER_TRAFFIC_JAM
                )
                // Set the expiration duration of the geofence. This geofence gets automatically
                // removed after this period of time.
                .setExpirationDuration(Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)

                // Set the transition types of interest. Alerts are only generated for these
                // transition. We track entry and exit transitions in this sample.
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                // Create the geofence.
                .setNotificationResponsiveness(1)
                .build());
    }

    /**
     * Extracts the accidents from the firebase message data.
     * @param remoteMessageData firebase data josn
     * @return The list of accidents
     */
    private List<Accident> getAccidents(Map<String, String> remoteMessageData) {
        List<Accident> accidentList = new ArrayList<>();
        String accidentDataAsJson = remoteMessageData.get("accidentList");
        Log.d(TAG, "Received accidents as json: " + accidentDataAsJson);
        if(accidentDataAsJson != null && !accidentDataAsJson.isEmpty()) {
            if (!accidentDataAsJson.startsWith("[") && !accidentDataAsJson.endsWith("]")) {
                accidentDataAsJson = "[" + accidentDataAsJson + "]";
            }
            Gson gson = new Gson();
            Type collectionType = new TypeToken<List<Accident>>() {
            }.getType();
            accidentList = gson.fromJson(accidentDataAsJson, collectionType);
            /*for (Accident currentAccident : accidentList) {
                currentAccident.getTimeInMilliSec();
            }*/
            return accidentList;
        }
        return accidentList;
    }

    /**
     * Create and show a notification containing the need of a emergency corridor.
     *
     * @param messageBody FCM message body received.
     * @param title FCM title recived
     */
    private void sendNotification(String title, String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.emergency_corridor_icon_bw)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    /**
     * Checks whether or not the app is in background.
     * @return boolean
     */
    private boolean isAppIsInBackground() {
        boolean isInBackground = true;
        Context context = getApplicationContext();
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    /**
     * This method announce the reminder for building a emergency corridor.
     * @param speechString message to announce
     */
    public void announceCorridorMessage(final String speechString) {
        Log.d("InformationActivity", "application context\n" + getApplicationContext());
        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                    textToSpeech.speak(speechString, TextToSpeech.QUEUE_FLUSH, null);
                } else {
                    Log.e(TAG, "TextToSpeech not successful");
                }
            }
        });
    }

    /**
    * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
    * Also specifies how the geofence notifications are initially triggered.
     */
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(geofenceAccidentList);


        // Return a GeofencingRequest.
        return builder.build();
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private PendingIntent getGeofencePendingIntent() {
        if (geofencePendingIntent != null) {
            return geofencePendingIntent;
        }
        Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
        geofencePendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return geofencePendingIntent;
    }
}
