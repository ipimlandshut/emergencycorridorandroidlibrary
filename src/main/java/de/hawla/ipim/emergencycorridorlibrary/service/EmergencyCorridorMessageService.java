package de.hawla.ipim.emergencycorridorlibrary.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hawla.ipim.emergencycorridorlibrary.model.Accident;
import de.hawla.ipim.emergencycorridorlibrary.model.Constants;

/**
 * Created by Julian on 05.08.2017.
 *
 * This class provides the Emergency Corridor service by starting or stopping to receive and announce the messages.
 *
 */

public class EmergencyCorridorMessageService {

    private static final String TAG = "EmegCorridorMsgService";

    public EmergencyCorridorMessageService() {
    }

    /**
     * This method starts the receiving of the emergency messages and therefore the Emergency Corridor Message Service.
     */
    public void receiveMessages(){
        FirebaseMessaging.getInstance().subscribeToTopic(Constants.FIREBASE_TOPIC);
    }

    /**
     * This method stop the receiving of the emergency messages and clears the geofences from the
     * service and clears geofence storage.
     * @param context the application context to clean the geofences
     */
    public void stopReceivingMessages(Context context){
        FirebaseMessaging.getInstance().unsubscribeFromTopic(Constants.FIREBASE_TOPIC);
        HashMap<String, Accident> accidentsMappedAsGeofences = getAccidentsFromPrefs(context);
        if(!accidentsMappedAsGeofences.isEmpty()){
            GeofencingClient geofencingClient = LocationServices.getGeofencingClient(context);
            List<String> geofenceListToRemove = new ArrayList<>(accidentsMappedAsGeofences.keySet());
            geofencingClient.removeGeofences(geofenceListToRemove);
            accidentsMappedAsGeofences.clear();
            addGeofencesToMap(context, accidentsMappedAsGeofences);
        }
    }

    static HashMap<String, Accident> getAccidentsFromPrefs(Context applicationContext){
        HashMap<String, Accident> accidentList = new HashMap<>();
        SharedPreferences sharedPreferences = applicationContext.getSharedPreferences(Constants.SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        String accidentsAsJson = sharedPreferences.getString(Constants.ACCIDENTS_MAPPED_AS_GEOFENCE_LIST, "");
        Log.d(TAG, "accidents as json from the shared prefs: " + accidentsAsJson);
        if(!accidentsAsJson.isEmpty()){
            Gson gson = new Gson();
            Type type = new TypeToken<HashMap<String, Accident>>(){}.getType();
            accidentList = gson.fromJson(accidentsAsJson, type);
            return clearGeoFenceMapFromOldEntries(accidentList);
        }
        return accidentList;
    }

    /**
     * This method clears all accidents from the accident list which are older than the geofence expiration date.
     */
    private static HashMap<String, Accident> clearGeoFenceMapFromOldEntries(HashMap<String, Accident> accidentsMappedAsGeofences){
        HashMap<String, Accident> newMap = new HashMap<>(accidentsMappedAsGeofences);
        Log.d(TAG, "Accident list size: " + accidentsMappedAsGeofences.size());
        for(String currentKey: accidentsMappedAsGeofences.keySet()){
            Accident currentAccident = accidentsMappedAsGeofences.get(currentKey);
            if (System.currentTimeMillis() - currentAccident.getTimeInMilliSec() > Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS){
                newMap.remove(currentKey);
                Log.d(TAG, "Removed accident from map:" + currentAccident);
            }
        }
        Log.d(TAG, "Accidents list size after clearing: " + newMap.size());
        return newMap;
    }

    static void addGeofencesToMap(Context context, HashMap<String, Accident> accidentsMappedAsGeofences) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREF_FILE_NAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        sharedPreferences.edit().putString(Constants.ACCIDENTS_MAPPED_AS_GEOFENCE_LIST, gson.toJson(accidentsMappedAsGeofences)).apply();
    }
}
