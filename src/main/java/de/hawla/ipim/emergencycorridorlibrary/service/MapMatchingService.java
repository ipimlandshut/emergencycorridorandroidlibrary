package de.hawla.ipim.emergencycorridorlibrary.service;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import de.hawla.ipim.emergencycorridorlibrary.model.Constants;
import de.hawla.ipim.emergencycorridorlibrary.model.GPSLocation;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Julian on 06.06.2017.
 * This class provides the map matching algorithm.
 */

class MapMatchingService {

    private static final String TAG = "MapMatchingService";
    private static final MediaType JSON =  MediaType.parse("application/json; charset=utf-8");


    static String getStreetFromGPSPositions(List<GPSLocation> gpsLocations) {
        Gson gson = new Gson();
        String json = gson.toJson(gpsLocations);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .build();
        RequestBody requestBody = RequestBody.create(JSON, json);
        Log.d(TAG, "getStreet json: " + json);

        Request request = new Request.Builder()
                .url(Constants.URI + "/getStreet")
                .post(requestBody)
                .build();
        for(int retries = 0; retries < 5; retries++) {
            try {
                Response response = client.newCall(request).execute();
                ResponseBody responseBody = response.body();
                if(responseBody != null) {
                    String jsonBody = responseBody.string();
                    Log.i(TAG, "response body from server: " + jsonBody);
                    JsonParser jsonParser = new JsonParser();
                    JsonObject jsonObject = jsonParser.parse(jsonBody).getAsJsonObject();
                    return jsonObject.get(Constants.JSON_STREET_NAME_OBJECT).getAsString();
                }
            } catch (IOException e) {
                Log.e(TAG, e.toString(), e);
                Log.e(TAG, "Exception thrown maybe no internet connection?");
            }  catch (JsonSyntaxException e) {
                Log.w(TAG, e.toString(), e);
                Log.w(TAG, "Exception thrown because of a male formed json.");
            }
        }
        return "";
    }


}
