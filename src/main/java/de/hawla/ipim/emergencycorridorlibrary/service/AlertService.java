package de.hawla.ipim.emergencycorridorlibrary.service;

import android.location.Location;
import android.util.Log;

import java.util.List;

import de.hawla.ipim.emergencycorridorlibrary.model.Accident;
import de.hawla.ipim.emergencycorridorlibrary.model.Constants;
import de.hawla.ipim.emergencycorridorlibrary.model.GPSLocation;
import de.hawla.ipim.emergencycorridorlibrary.model.UserData;

/**
 * Created by Julian on 30.06.2017.
 * This class determines whether an user should be advised to form a emergency corridor or not.
 */

public class AlertService {

    private static final String TAG = "AlertService";

    /**
     * This method proves whether or not the user is relevant for the accident.
     * @param accident the accident object
     * @param userData the user data
     * @return boolean if true the user is relevant if false he is not.
     */
    public boolean isUserRelevantForAccident(Accident accident,UserData userData){
        List<GPSLocation> userLocations = userData.getUserLocations();
        boolean isUserCloseToAccident = isUserCloseToAccidentArea(accident, userLocations.get(userLocations.size()-1));
        Log.d(TAG, "Is user close to accident " + accident.getId() + " : " + isUserCloseToAccident);
        if(isUserCloseToAccident){
            boolean isUserOnSameStreet = isUserOnSameStreetAsAccident(accident, userLocations);
            Log.d(TAG, "Is user on the same street as the accident  " + accident.getId() + " : " + isUserOnSameStreet);
            if(isUserOnSameStreet) {
                boolean isUserApproachingFromRightDirection = isUserApproachingFromTheRightDirection(accident, userData);
                Log.d(TAG, "Is user approaching from the right direction and relevant for the accident  " + accident.getId() + " : " + isUserApproachingFromRightDirection);
                if (isUserApproachingFromRightDirection) {
                    return true;
                }
            }
        }
        return false;

    }

    private boolean isUserApproachingFromTheRightDirection(Accident accident, UserData userData) {
        List<GPSLocation> userLocations = userData.getUserLocations();
        float[] lastKnownDistanceUserToAccident = new float[1];
        float[] lastKnownDistanceUserToEndOfTrafficJam = new float[1];
        float[] firstKnownDistanceToAccident = new float[1];
        float[] penultimateKnownDistanceUserToAccident = new float[1];
        float[] penultimateKnownDistanceUserToEndOfTrafficJam = new float[1];


        GPSLocation usersLastKnownLocation = userLocations.get(userLocations.size()-1);
        GPSLocation usersFirstKnownLocation = userLocations.get(0);
        GPSLocation usersPenultimateKnownLocation = userLocations.get(userLocations.size()-2);
        Location.distanceBetween(accident.getAccidentLocation().getLatitude(),accident.getAccidentLocation().getLongitude(), usersLastKnownLocation.getLatitude(), usersLastKnownLocation.getLongitude(), lastKnownDistanceUserToAccident);
        Location.distanceBetween(accident.getTrafficJamEndLocation().getLatitude(),accident.getTrafficJamEndLocation().getLongitude(), usersLastKnownLocation.getLatitude(), usersLastKnownLocation.getLongitude(), lastKnownDistanceUserToEndOfTrafficJam);
        Location.distanceBetween(accident.getAccidentLocation().getLatitude(), accident.getAccidentLocation().getLongitude(), usersFirstKnownLocation.getLatitude(), usersFirstKnownLocation.getLongitude(), firstKnownDistanceToAccident);
        Location.distanceBetween(accident.getAccidentLocation().getLatitude(),accident.getAccidentLocation().getLongitude(), usersPenultimateKnownLocation.getLatitude(), usersPenultimateKnownLocation.getLongitude(), penultimateKnownDistanceUserToAccident);
        Location.distanceBetween(accident.getTrafficJamEndLocation().getLatitude(), accident.getTrafficJamEndLocation().getLongitude(), usersPenultimateKnownLocation.getLatitude(), usersPenultimateKnownLocation.getLongitude(), penultimateKnownDistanceUserToEndOfTrafficJam);

        if(lastKnownDistanceUserToAccident[0]>lastKnownDistanceUserToEndOfTrafficJam[0] && firstKnownDistanceToAccident[0]>lastKnownDistanceUserToAccident[0]){
            return true;
        } else if(penultimateKnownDistanceUserToEndOfTrafficJam[0] < lastKnownDistanceUserToEndOfTrafficJam[0] && penultimateKnownDistanceUserToAccident[0] > lastKnownDistanceUserToAccident[0]){
            return true;
        }
        return false;
    }

    private boolean isUserOnSameStreetAsAccident(Accident accident, List<GPSLocation> userLocations) {
        String accidentStreet = accident.getStreet();
        String userStreet = MapMatchingService.getStreetFromGPSPositions(userLocations);
        Log.d(TAG, "user Street: " + userStreet + "; accident street: " + accidentStreet);
        if(accidentStreet.isEmpty() || userStreet.isEmpty()){
            return false;
        }
        return userStreet.toLowerCase().contains(accidentStreet.toLowerCase());

    }

    /**
     * Proofs whether or not the user is close to the accident area.
     * @param accident the accident to proof
     * @param userLocation the current gps position of the user
     * @return boolean true or false as answer.
     */
    public boolean isUserCloseToAccidentArea(Accident accident, GPSLocation userLocation) {

        GPSLocation accidentLocation = accident.getAccidentLocation();
        float distanceUserToAccident[] = new float[1];
        Location.distanceBetween(userLocation.getLatitude(), userLocation.getLongitude(), accidentLocation.getLatitude(), accidentLocation.getLongitude(), distanceUserToAccident);
        return accident.getDistanceBetweenAccidentAndEndOfTrafficJam()+ Constants.WARNING_AREA_AFTER_TRAFFIC_JAM > distanceUserToAccident[0];

    }

}
