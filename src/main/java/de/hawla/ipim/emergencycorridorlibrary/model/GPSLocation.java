package de.hawla.ipim.emergencycorridorlibrary.model;

import android.location.Location;

/**
 * Created by Julian on 06.06.2017.
 * POJO for the GPSLocations
 */

public class GPSLocation {


    private double latitude;
    private double longitude;
    private long timeInMilliSec;


    public float distanceTo(GPSLocation other) {
        Location ownLoc = new Location("");
        ownLoc.setLatitude(latitude);
        ownLoc.setLongitude(longitude);
        Location otherLoc= new Location("");
        otherLoc.setLatitude(other.latitude);
        otherLoc.setLongitude(other.longitude);

        return ownLoc.distanceTo(otherLoc);
    }
    @Override
    public String toString() {
        return "GPSLocation{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", timeInMilliSec=" + timeInMilliSec +
                '}';
    }

    public GPSLocation(double latitude, double longitude, long timeInMilliSec) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.timeInMilliSec = timeInMilliSec;
    }

    public GPSLocation(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public GPSLocation() {
    }

    public long getTimeInMilliSec() {
        return timeInMilliSec;
    }

    public void setTimeInMilliSec(long timeInMilliSec) {
        this.timeInMilliSec = timeInMilliSec;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


}
