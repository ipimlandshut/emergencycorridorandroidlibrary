/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.hawla.ipim.emergencycorridorlibrary.model;

/**
 * Constants used in this library.
 */

public final class Constants {



    private Constants() {
    }

    /**
     * Time to wait before the next gps point will be evaluated in milli sec
     */
    public static long minTime = 500;
    /**
     * Distance to cover before the next gps point can be evaluated
     */
    public static float minDistance = 0;
    /**
     * URL to the backend server for the data transmission and map matching
     */
    public static final String URI = "http://rgasse.bayerninfo.de:2880/EmergencyCorridorServer/rest/services";
    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    /**
     * warning area before the end of traffic jam in meters
     */
    public static final float WARNING_AREA_AFTER_TRAFFIC_JAM = 5000f;


    /**
     * Used to set an expiration time for a geofence. After this amount of time Location Services
     * stops tracking the geofence.
     */
    private static final long GEOFENCE_EXPIRATION_IN_HOURS = 3;

    /**
     * Topic from which the app will receive firebase messages.
     */
    public static final String FIREBASE_TOPIC = "EmergencyCorridor";

    /**
     * Expiration time for the geofences in milli secs
     */
    public static final long GEOFENCE_EXPIRATION_IN_MILLISECONDS = GEOFENCE_EXPIRATION_IN_HOURS * 60 * 60 * 1000;

    /**
     * Josn identifier to get the street name from the json object.
     */
    public static final String JSON_STREET_NAME_OBJECT = "streetName";

    /**
     * Identifier for the accident list in the shared preferences.
     */
    public static final String ACCIDENTS_MAPPED_AS_GEOFENCE_LIST = "accidentsMappedAsGeofencesJson";

    /**
     * Identifier for the shared preference file
     */
    private static final String PACKAGE_NAME = "de.hawla.ipim.emergencycorridorlibrary";
    public static final String SHARED_PREF_FILE_NAME = PACKAGE_NAME + ".SHARED_PREF_FILE";
}
