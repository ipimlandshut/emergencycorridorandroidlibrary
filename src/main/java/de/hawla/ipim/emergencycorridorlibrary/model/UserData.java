package de.hawla.ipim.emergencycorridorlibrary.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Julian on 30.06.2017.
 * POJO for user data.
 */

public class UserData {
    private List<GPSLocation> userLocations;
    private float speed;

    @Override
    public String toString() {
        return "UserData{" +
                "userLocations=" + userLocations +
                ", speed=" + speed +
                '}';
    }

    public UserData() {
        userLocations = new ArrayList<>();
    }

    public UserData(List<GPSLocation> userLocations, float speed) {
        this.userLocations = userLocations;
        this.speed = speed;
    }

    public List<GPSLocation> getUserLocations() {

        return userLocations;
    }

    public void setUserLocations(List<GPSLocation> userLocations) {
        this.userLocations = userLocations;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }
}
