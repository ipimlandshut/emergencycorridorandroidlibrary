package de.hawla.ipim.emergencycorridorlibrary.model;

import android.location.Location;

/**
 * Created by Julian on 30.06.2017.
 */

public class Accident {

    private String id;
    private GPSLocation accidentLocation;
    private GPSLocation trafficJamEndLocation;
    private float distanceBetweenAccidentAndEndOfTrafficJam;
    private String street;
    private long timeInMilliSec;
    private String exitNearbyAccident;
    private String exitNearbyJamEnd;


    public Accident(GPSLocation accidentLocation, GPSLocation trafficJamEndLocation) {
        this.accidentLocation = accidentLocation;
        this.trafficJamEndLocation = trafficJamEndLocation;
    }

    public Accident(String id, GPSLocation accidentLocation, GPSLocation trafficJamEndLocation, float distanceBetweenAccidentAndEndOfTrafficJam, String street, long timeInMilliSec, String exitNearbyAccident, String exitNearbyJamEnd) {
        this.id = id;
        this.accidentLocation = accidentLocation;
        this.trafficJamEndLocation = trafficJamEndLocation;
        this.distanceBetweenAccidentAndEndOfTrafficJam = distanceBetweenAccidentAndEndOfTrafficJam;
        this.street = street;
        this.timeInMilliSec = timeInMilliSec;
        this.exitNearbyAccident = exitNearbyAccident;
        this.exitNearbyJamEnd = exitNearbyJamEnd;
    }

    public void setDistanceBetweenAccidentAndEndOfTrafficJam(float distanceBetweenAccidentAndEndOfTrafficJam) {
        this.distanceBetweenAccidentAndEndOfTrafficJam = distanceBetweenAccidentAndEndOfTrafficJam;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public long getTimeInMilliSec() {
        return timeInMilliSec;
    }

    public void setTimeInMilliSec(long timeInMilliSec) {
        this.timeInMilliSec = timeInMilliSec;
    }

    public String getExitNearbyAccident() {
        return exitNearbyAccident;
    }

    public void setExitNearbyAccident(String exitNearbyAccident) {
        this.exitNearbyAccident = exitNearbyAccident;
    }

    public String getExitNearbyJamEnd() {
        return exitNearbyJamEnd;
    }

    public void setExitNearbyJamEnd(String exitNearbyJamEnd) {
        this.exitNearbyJamEnd = exitNearbyJamEnd;
    }

    public GPSLocation getAccidentLocation() {
        return accidentLocation;
    }

    public void setAccidentLocation(GPSLocation accidentLocation) {
        this.accidentLocation = accidentLocation;
    }

    public GPSLocation getTrafficJamEndLocation() {
        return trafficJamEndLocation;
    }

    public void setTrafficJamEndLocation(GPSLocation trafficJamEndLocation) {
        this.trafficJamEndLocation = trafficJamEndLocation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getDistanceBetweenAccidentAndEndOfTrafficJam(){
        if(distanceBetweenAccidentAndEndOfTrafficJam == 0.0f) {
            float[] result = new float[1];
            Location.distanceBetween(accidentLocation.getLatitude(), accidentLocation.getLongitude(), trafficJamEndLocation.getLatitude(), trafficJamEndLocation.getLongitude(), result);
            distanceBetweenAccidentAndEndOfTrafficJam = result[0];
        }
        return distanceBetweenAccidentAndEndOfTrafficJam;
    }

    @Override
    public String toString() {
        return "Accident{" +
                "id='" + id + '\'' +
                ", accidentLocation=" + accidentLocation +
                ", trafficJamEndLocation=" + trafficJamEndLocation +
                ", distanceBetweenAccidentAndEndOfTrafficJam=" + distanceBetweenAccidentAndEndOfTrafficJam +
                ", street='" + street + '\'' +
                ", timeInMilliSec=" + timeInMilliSec +
                ", exitNearbyAccident='" + exitNearbyAccident + '\'' +
                ", exitNearbyJamEnd='" + exitNearbyJamEnd + '\'' +
                '}';
    }
}
